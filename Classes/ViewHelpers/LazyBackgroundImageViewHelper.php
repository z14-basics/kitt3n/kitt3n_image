<?php

namespace KITT3N\Kitt3nImage\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;

/**
 * Class LazyImageViewHelper
 * @package LITT3N\Kitt3nImage\ViewHelpers
 */
class LazyBackgroundImageViewHelper extends AbstractTagBasedViewHelper
{
    /**
     * translationService
     *
     * @var \KITT3N\Kitt3nImage\Service\ProcessService
     * @inject
     */
    protected $processService = null;

    /**
     * lazyImageService
     *
     * @var \KITT3N\Kitt3nImage\Service\LazyBackgroundImageService
     * @inject
     */
    protected $lazyBackgroundImageService = null;

    /**
     * @param \TYPO3\CMS\Extbase\Service\ImageService $imageService
     */
    public function injectImageService(\TYPO3\CMS\Extbase\Service\ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    public function initializeArguments()
    {
        $this->registerArgument('coreFileReference', 'object', 'Core File Reference', true);
        $this->registerArgument('fileReferenceUid', 'integer', 'Core File Reference uid', true);
    }

    /*
     * Use in fluid template or partial:
     *
     * Namespace:
     * e.g. {namespace kitt3nImage=KITT3N\Kitt3nImage\ViewHelpers}
     *
     * Content:
     * e.g. <kitt3nImage:lazyBackgroundImage coreFileReference="{processedImage.0}" fileReferenceUid="{processedImage.0.uid}"/>
     */
    public function render()
    {
        $sHtml = "";

        $aArguments = $this->arguments;

        $sHtml .= $this->lazyBackgroundImageService->returnLazyBackgroundImageHtml($aArguments);

        return $sHtml;

    }

}