<?php

namespace KITT3N\Kitt3nImage\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;

/**
 * Class ImageViewHelper
 * @package LITT3N\Kitt3nImage\ViewHelpers
 */
class ImageViewHelper extends AbstractTagBasedViewHelper
{
    /**
     * translationService
     *
     * @var \KITT3N\Kitt3nImage\Service\ProcessService
     * @inject
     */
    protected $processService = null;

    /**
     * @param \TYPO3\CMS\Extbase\Service\ImageService $imageService
     */
    public function injectImageService(\TYPO3\CMS\Extbase\Service\ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    public function initializeArguments()
    {
        $this->registerArgument('coreFileReference', 'object', 'Core File Reference', true);
        $this->registerArgument('fileReferenceUid', 'integer', 'Core File Reference uid', true);
    }

    /*
     * Use in fluid template or partial:
     *
     * Namespace:
     * e.g. {namespace kitt3nImage=KITT3N\Kitt3nImage\ViewHelpers}
     *
     * Content:
     * e.g. <kitt3nImage:image coreFileReference="{processedImage.0}" fileReferenceUid="{processedImage.0.uid}"/>
     */
    public function render()
    {
        $sHtml = "";

        $aArguments = $this->arguments;

        $aProcessed = $this->processService->process($aArguments);

        if ( ! null == $aProcessed) {

            $aProcessedImageClasses = [];
            foreach ($aProcessed[1] as $iProcessedImage => $aProcessedImage) {
                $aProcessedImageClasses[] = $aProcessedImage["classes"];
            }

            $sHtml .=
                '<picture 
                        class="' . implode(' ', $aProcessedImageClasses) . '">
                        <source 
                            media="(min-width: 1200px)" 
                            srcset="' . (array_key_exists("frontendPathSymlink", $aProcessed[1][0]) ?
                                $aProcessed[1][0]["frontendPathSymlink"] : $aProcessed[1][0]["frontendPath"]) . '">
                        <source 
                            media="(min-width: 768px)" 
                            srcset="' . (array_key_exists("frontendPathSymlink", $aProcessed[1][1]) ?
                                $aProcessed[1][1]["frontendPathSymlink"] : $aProcessed[1][1]["frontendPath"]) . '">
                        <img 
                            src="' . (array_key_exists("frontendPathSymlink", $aProcessed[1][2]) ?
                                $aProcessed[1][2]["frontendPathSymlink"] : $aProcessed[1][02]["frontendPath"]) . '" 
                            alt="' . $aProcessed[0]->getAlternative() . '" 
                            title="' . $aProcessed[0]->getTitle() . '" 
                            style="width:auto;max-width:100%;">
                        <noscript>
                            <img 
                                src="' . (array_key_exists("frontendPathSymlink", $aProcessed[1][2]) ?
                                    $aProcessed[1][2]["frontendPathSymlink"] : $aProcessed[1][2]["frontendPath"]) . '" 
                                alt="' . $aProcessed[0]->getAlternative() . '" 
                                title="' . $aProcessed[0]->getTitle() . '" 
                                style="width:auto;max-width:100%;">
                        </noscript>
                    </picture>';

        } else {
            $sHtml .= "Error";
        }

        return $sHtml;

    }

}